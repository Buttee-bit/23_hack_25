import PyPDF2 # pip install 'PyPDF2<3.0'
import re
import string
import nltk
import pymorphy3
from nltk.corpus import stopwords

class Preprocessing():
    def __init__(self):

        self.spec_chars = string.punctuation + r'\n\x0«»\t—…[]\n*'
        self.stop_words = stopwords.words('russian')
        self.morph = pymorphy3.MorphAnalyzer()

    def pdf_converter(self, list_of_docs):
        
        self.converted_texts = []

        for path_to_pdf in list_of_docs:
            # Открываем PDF-файл
            with open(path_to_pdf, 'rb') as pdf_file:
                # Создаем объект PDF-документа
                pdf_reader = PyPDF2.PdfFileReader(pdf_file)

                # Получаем количество страниц в PDF-документе
                num_pages = pdf_reader.numPages

                # Создаем пустую строку для хранения текста
                text = ""

                # Проходим по всем страницам PDF-документа
                for page in range(num_pages):
                    # Получаем объект страницы
                    pdf_page = pdf_reader.getPage(page)

                    # Извлекаем текст из страницы
                    page_text = pdf_page.extractText()

                    # Добавляем текст страницы в общую строку
                    text += page_text

            self.converted_texts.append(text)

            # Выводим полученный текст
            # print(text)

    def preprocessing(self, converted_texts):
        self.final_texts = []
        for converted_text in converted_texts:
            if converted_text is None:
                pass

            if type(converted_text) != float:
                converted_text = "".join([ch for ch in converted_text if ch not in self.spec_chars])
                converted_text = re.sub('\n', '     ', converted_text)
                tokens = nltk.word_tokenize(converted_text)
                filtered_text = [word.lower() for word in tokens if word.lower() not in self.stop_words]
                self.final_text = []

                for word in filtered_text:
                    if word.isalpha() and len(word) > 2:
                        parsed_word = self.morph.parse(word)[0]
                        pos = parsed_word.tag.POS
                        self.final_text.append(f'{parsed_word.normal_form}_{pos}')
                    else:
                        continue
            
            self.final_texts.append(self.final_text)

    def main(self):
        list_of_docs = [r'nlp\pdf_docs\test_pdf.pdf', r'nlp\pdf_docs\test2.pdf', r'nlp\pdf_docs\kotenok.pdf', r'nlp\pdf_docs\test_pdf.pdf']
        # list_of_docs = [
        #     \r'nlp\pdf_docs\test2.pdf', r'nlp\pdf_docs\kotenok.pdf']
        self.pdf_converter(list_of_docs)
        self.preprocessing(self.converted_texts)
        # print(self.final_texts)


if __name__ == '__main__':
    example = Preprocessing()
    example.main()

        
import gensim

# Загрузка предобученной модели Word2Vec
model_path = "nlp\models\model.bin"
model = gensim.models.KeyedVectors.load_word2vec_format(model_path, binary=True)

# Получение векторного представления слова
vector = model["приказ_NOUN"]
print("Vector shape:", vector.shape)
print("Vector:", vector)

# Поиск наиболее похожих слов
# similar_words = model.most_similar("утверждение_NOUN")
# print("Similar words:", similar_words)

# # Выполнение алгебраических операций с векторами слов
# result = model.most_similar(positive=["king", "woman"], negative=["man"], topn=1)
# print("Result:", result)
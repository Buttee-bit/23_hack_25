import PyPDF2 # pip install 'PyPDF2<3.0'

class PDF_to_text():
    def __init__(self, path_to_pdf):
        # Открываем PDF-файл
        with open(path_to_pdf, 'rb') as pdf_file:
            # Создаем объект PDF-документа
            pdf_reader = PyPDF2.PdfFileReader(pdf_file)

            # Получаем количество страниц в PDF-документе
            num_pages = pdf_reader.numPages

            # Создаем пустую строку для хранения текста
            text = ""

            # Проходим по всем страницам PDF-документа
            for page in range(num_pages):
                # Получаем объект страницы
                pdf_page = pdf_reader.getPage(page)

                # Извлекаем текст из страницы
                page_text = pdf_page.extractText()

                # Добавляем текст страницы в общую строку
                text += page_text

        # Выводим полученный текст
        # print(text)
        return text

example = PDF_to_text(r'nlp\test_pdf.pdf')
import PyPDF2 # pip install 'PyPDF2<3.0'
from fuzzywuzzy import fuzz
from fuzzywuzzy import process

class PDF_to_text():
    def __init__(self, path_to_pdf, etalon):
        # Открываем PDF-файл
        with open(path_to_pdf, 'rb') as pdf_file:
            # Создаем объект PDF-документа
            pdf_reader = PyPDF2.PdfFileReader(pdf_file)

            num_pages = pdf_reader.numPages
            
            if num_pages >= 15:
                for page in range(15):
                    text = ""
                    pdf_page = pdf_reader.getPage(page)
                    page_text = pdf_page.extractText()
                    text += page_text

                    text = text.split('\n')

                    for element in text:
                        fuzz_rate = fuzz.UWRatio(etalon, element)
                        if fuzz_rate >= 80:
                            print(element, page, fuzz_rate)
            else:
                for page in range(num_pages):
                    text = ""
                    pdf_page = pdf_reader.getPage(page)
                    page_text = pdf_page.extractText()
                    text += page_text

                    text = text.split('\n')

                    for element in text:
                        fuzz_rate = fuzz.UWRatio(etalon, element)
                        if fuzz_rate >= 80:
                            print(element, page, fuzz_rate)

            

        # Выводим полученный текст
        # print(text)
        # return text

example = PDF_to_text(r'backend\src\api\INPUT_\╨а╨░╨╖╨┤╨╡╨╗ ╨Я╨Ф тДЦ3_╨в╨╛╨╝ 3.1_51-╨в╨Ъ╨а..00027-21_╨б╨Ъ╨н-25982.pdf', 'Капитальный ремонт автомобильной дороги Р-215 Астрахань - Кочубей - Кизляр - Махачкала, подъезд к г. Грозный на участке км 70+127 – км 85+267, Чеченская Республика')

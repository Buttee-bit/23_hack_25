# import pickle

# with open("all_texts", "rb") as fp:
#     b = pickle.load(fp)

# print(len(b))

# import pathlib
# all_paths = []

# directory = r'C:\Users\Chubu\OneDrive\Рабочий стол\DigBr2\23_hack_25\Документация 1'
# for filepath in pathlib.Path(directory).glob('**/*'):
#     all_paths.append(str(filepath.absolute()))

# directory = r'C:\Users\Chubu\OneDrive\Рабочий стол\DigBr2\23_hack_25\Документация 2'
# for filepath in pathlib.Path(directory).glob('**/*'):
#     all_paths.append(str(filepath.absolute()))

# directory = r'C:\Users\Chubu\OneDrive\Рабочий стол\DigBr2\23_hack_25\ПД'
# for filepath in pathlib.Path(directory).glob('**/*'):
#     all_paths.append(str(filepath.absolute()))

# print(len(all_paths))
# print(all_paths)

# import PyPDF2

# def find_highlighted_text(pdf_path):
#     # Открытие PDF файла
#     with open(pdf_path, 'rb') as file:
#         pdf_reader = PyPDF2.PdfReader(file)

#         # Поиск выделенных фрагментов
#         highlighted_text = []
#         for page in pdf_reader.pages:
#             if '/Annots' in page:
#                 annotations = page['/Annots']
#                 for annotation in annotations:
#                     if annotation.getObject()['/Subtype'] == '/Highlight':
#                         rect = annotation.getObject()['/Rect']
#                         quad_points = annotation.getObject()['/QuadPoints']
#                         content =  annotation.getObject()['/Contents']
                        
#                         highlighted_text.append({
#                             'rect': rect,
#                             'quad_points': quad_points,
#                             'content': content
#                         })
#     return highlighted_text

# # Пример использования
# pdf_path = r'ПД\896-0-ИЭИ-1 изм.2.01499-19_ГГЭ-17661.pdf'
# highlighted_text = find_highlighted_text(pdf_path)
# for text in highlighted_text:
#     print("Выделенный фрагмент:")
#     print("Прямоугольник:", text['rect'])
#     print("Координаты точек:", text['quad_points'])
#     print("Содержимое:", text['content'])
#     print("---")


# [{'pdf_name': 'qwerty1.pdf', 'amount': [{'page_num': 1, 'rate': 95, 'description': 'Скважина1'},
#                                            {'page_num': 2, 'rate': 98, 'description': 'Скважина2'},
#                                            {'page_num': 3, 'rate': 90, 'description': 'Скважина3'}]},
#  {'pdf_name': 'qwerty2.pdf', 'amount': [{'page_num': 5, 'rate': 91, 'description': 'Скважина11'},
#                                            {'page_num': 8, 'rate': 93, 'description': 'Скважина22'},
#                                            {'page_num': 47, 'rate': 99, 'description': 'Скважина33'}]}]

import PyPDF2, os

def extract_pages(input_path, output_path, page_numbers):
    pdf_reader = PyPDF2.PdfFileReader(open(input_path, 'rb'))
    pdf_writer = PyPDF2.PdfFileWriter()

    for page_num in page_numbers:
        page = pdf_reader.getPage(page_num - 1)  # Индексы страниц начинаются с 0, поэтому вычитаем 1
        pdf_writer.addPage(page)

    with open(output_path, 'wb') as output_file:
        pdf_writer.write(output_file)

# Пример использования
input_path = r'backend\src\api\INPUT_\╨а╨░╨╖╨┤╨╡╨╗_╨Я╨Ф_тДЦ1_╨в╨╛╨╝_1_51_╨Я╨Ч_00027_21_╨б╨Ъ╨н_25982.pdf'
output_path = 'output.pdf'
page_numbers = [1, 4, 7]

extract_pages(input_path, output_path, page_numbers)

os.startfile('example_highlighted.pdf')
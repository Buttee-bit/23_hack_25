import sqlite3
import json
from datetime import datetime
class DataBase:

    def __init__(self):
        self.conn = sqlite3.connect('database.db')
        self.cur = self.conn.cursor()

        self.cur.execute("""CREATE TABLE IF NOT EXISTS ideality(
           IDEAL_ID INTEGER PRIMARY KEY,
           ROW_TEXT TEXT,
           ADV_TEXT TEXT,
           DOC_VECTOR TEXT
           );
        """)

        self.cur.execute("""CREATE TABLE IF NOT EXISTS vectors(
           VECTOR_ID INTEGER PRIMARY KEY,
           DOC_VECTOR TEXT
           );
        """)

        self.cur.execute("""CREATE TABLE IF NOT EXISTS requests(
           REQUEST_ID INTEGER PRIMARY KEY,
           CREATED_AT TEXT DEFAULT (datetime('now')),
           REQUEST_ROW_TEXT TEXT,
           REQUEST_ADV_TEXT TEXT,
           REQUEST_DOC_VECTOR TEXT
           );
        """)

        self.conn.commit()

################################### ideality #########################################

    def insert_into_db(self, new_line):
        # new_line[3] = sqlite3.Binary(new_line[3])
        self.cur.execute("INSERT INTO ideality VALUES(NULL, ?, ?, ?);", new_line)
        self.conn.commit()

    def read_all_db(self):
        self.result = []
        self.cur.execute("SELECT * from ideality")
        records = self.cur.fetchall()
        for row in records:
            self.result.append({"IDEAL_ID": row[0],
                                "ROW_TEXT": row[1],
                                "ADV_TEXT": json.loads(row[2]),
                                "DOC_VECTOR": json.loads(row[3])})

    def db_cleaning(self):
        self.cur.execute("""DELETE FROM ideality;""")
        self.conn.commit()

################################### vectors #########################################

    def insert_vector_db(self, vector):
        self.cur.execute("INSERT INTO vectors VALUES(NULL, ?);", (vector,))
        self.conn.commit()

    def read_vectors_db(self):
        self.vectors = []
        self.cur.execute("SELECT VECTOR_ID, DOC_VECTOR from vectors")
        records = self.cur.fetchall()
        for row in records:
            self.vectors.append({"VECTOR_ID": row[0],
                                 "DOC_VECTOR": json.loads(row[1])})
        # print(self.vectors)

    def db_vectors_cleaning(self):
        self.cur.execute("""DELETE FROM vectors;""")
        self.conn.commit()

################################### requests #########################################

    def insert_requests_db(self, data):
        self.cur.execute("INSERT INTO requests(REQUEST_ROW_TEXT, REQUEST_ADV_TEXT, REQUEST_DOC_VECTOR) VALUES(?, ?, ?);", data)
        self.conn.commit()

    def read_requests_db(self):
        self.requests = []
        self.cur.execute("SELECT * from requests")
        records = self.cur.fetchall()
        for row in records:
            self.requests.append({"REQUEST_ID": row[0],
                                 "CREATED_AT": row[1],
                                 "REQUEST_ROW_TEXT": row[2],
                                 "REQUEST_ADV_TEXT": json.loads(row[3]),
                                 "REQUEST_DOC_VECTOR": json.loads(row[4])})
        print(self.requests)

    def db_requests_cleaning(self):
        self.cur.execute("""DELETE FROM requests;""")
        self.conn.commit()

if __name__ == '__main__':
    database = DataBase()
    # database.db_cleaning()
    # database.insert_vector_db(json.dumps([ 1.283796  ,  1.3905724 , -0.11916393,  0.53872657, -0.25801697,
    #     1.7587804 , -0.24140976, -0.77849054, -0.48930869,  0.34826338,
    #    -0.76693976,  2.0165958 ,  0.03670225, -0.04976864,  0.40761757,
    #     0.29272735,  0.6730231 , -0.32132676,  0.21853161,  1.0961515 ,
    #     1.6176819 , -0.85484976,  0.8867571 , -0.43600827,  0.04443426,
    #     0.8293013 , -1.5945643 ,  0.579109  , -1.5109699 ,  1.0733677 ,
    #     0.7257319 , -1.0317566 , -1.2080457 , -0.5314145 , -1.7991073 ,
    #    -0.6807175 , -0.7402437 , -1.1356837 ,  0.16527091,  0.16288844,
    #    -0.2820076 , -0.2778301 , -0.01796355, -1.2296709 , -1.5767971 ,
    #    -0.31131646,  0.16944252, -0.01683443, -0.09584506, -1.1923045 ,
    #    -0.29281744,  0.08354397, -0.19933495, -0.18725273,  0.13088134,
    #     1.7346951 , -1.5024282 ,  0.03156332, -0.11549279, -0.8412664 ,
    #    -0.94048053,  0.40699938, -0.90310854,  0.03384403, -0.69038266,
    #     1.2516457 , -0.04931705,  0.33228922,  0.14271067,  0.21682678,
    #    -1.199835  ,  0.62882364, -0.11102229, -0.10609503,  0.95279294,
    #     0.60017174,  0.45085093,  0.10866183, -0.39301062, -0.5713373 ,
    #    -0.66769516, -0.0917495 , -0.3179799 ,  0.34046444, -0.05499611,
    #     1.0345684 , -0.13350171, -0.8313331 , -0.5581369 , -0.01166003,
    #     2.0652206 ,  0.3692134 , -0.31734696, -0.62610006, -1.0926251 ,
    #    -0.19006893,  1.1663959 ,  0.86303777,  0.18405691,  0.06354696,
    #     0.97291565, -0.5117162 ,  0.903058  ,  2.1983297 , -1.5036378 ,
    #     0.17724714, -1.6837713 ,  0.5289055 , -1.2717752 , -0.7752319 ,
    #    -0.6936034 , -0.5740597 ,  0.7290608 , -0.15967575,  0.3900606 ,
    #     0.20811477, -0.06898433,  0.95376223,  1.6446733 , -1.0551373 ,
    #    -0.96696097,  0.9792867 ,  0.2986168 ,  0.15847315, -1.5543249 ,
    #    -1.8789829 ,  1.2529825 ,  0.23560634,  1.2583885 ,  1.0255529 ,
    #    -0.6953209 ,  0.6717749 , -1.0760884 , -1.4987698 ,  0.16808616,
    #    -0.78498185,  0.36794132, -0.44603077,  1.1551938 , -1.6694821 ,
    #    -0.33127296, -0.0228013 ,  0.23168382, -0.41672936, -0.45618033,
    #     0.95377904,  0.5970116 , -0.42800033,  1.2094463 , -0.8914331 ,
    #    -0.21542369, -1.9720864 ,  0.50358534, -0.14359373,  1.2735956 ,
    #     0.19904202,  0.14471236, -0.08726913, -1.6379404 ,  0.89639103,
    #    -2.1621733 , -0.5883681 , -0.7618667 ,  0.28847566,  0.24408127,
    #    -1.2767131 ,  0.6592177 , -1.2530497 , -1.0421811 , -0.33055568,
    #     0.29488227, -0.5124069 , -0.79270107, -0.3781299 ,  0.0092347 ,
    #    -0.0672733 ,  1.6921605 ,  0.04995083, -0.3031555 , -0.2904451 ,
    #    -1.3510866 ,  1.6264248 ,  1.2723279 , -0.3291652 , -0.67618096,
    #    -0.7191392 ,  0.3938243 ,  0.9086645 ,  0.17632356, -0.7673189 ,
    #    -0.43987757,  2.3088374 , -0.8570287 ,  0.967242  , -0.23020445,
    #     0.46093792, -1.3281507 , -0.5468254 ,  0.6663137 , -1.5266415 ,
    #    -0.22065109,  0.08297226, -1.4622084 ,  0.7512102 ,  0.07312396,
    #     1.036682  , -0.12043548, -0.38108042,  0.255269  , -0.35768798,
    #    -0.32774103, -1.6227361 , -0.03117337,  2.6636906 , -0.34061742,
    #    -0.08457241, -0.46663743, -1.1083442 , -0.58325076,  0.14006913,
    #    -0.9433535 ,  1.7910209 ,  0.9005622 ,  0.8749167 ,  0.9825504 ]))

    # database.db_vectors_cleaning()
    # database.read_vectors_db()
    # database.db_requests_cleaning()
    # database.insert_requests_db(['Some text', json.dumps(['Some', 'text']), json.dumps([2121, 2121])])
    # database.read_requests_db()
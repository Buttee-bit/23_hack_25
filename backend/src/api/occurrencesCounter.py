import json
import re
import sys, os
from os import walk

import PyPDF2  # pip install 'PyPDF2<3.0'
from fuzzywuzzy import fuzz, process


class OccurrencesCounter():
    STOP_SEPARATORS = [',',';'] # ,'.'
    SIMILARITY_LEVEL = 80
    ENTERY_POINT = sys.path[0]+'\\'+ 'INPUT_'
    
    pdfTitle = []
    jsonData : dict

    def __init__(self, pdfPath : str, pdfTitle : str) -> None:
        self.pdfTitle = OccurrencesCounter.multiSplit(pdfTitle, self.STOP_SEPARATORS)
        self.pdfTitle.append( pdfTitle )
        self.setJsonData(pdfPath)
        self.occurrencePDF(pdfPath)
        # OccurrencesCounter.add_json(filePath, [self.jsonData])
        # print('_____STOP_____')
    
    def getJsonData(self) -> dict:
        return self.jsonData

    def setEnteryPoint(self, enteryPoint : str) -> None:
        self.ENTERY_POINT = enteryPoint
    
    def setJsonData(self, pdfPath : str) -> None:
        for i in range(len( self.pdfTitle )):
            self.jsonData =  {"pdf_name": pdfPath, "content":[]} 
    
    def setStopSeparators(self, newSeparators : list) -> None:
        self.STOP_SEPARATORS = newSeparators
        
    def setSimilarityLevel(self, similarityLevel : int) -> None:
        self.SIMILARITY_LEVEL = similarityLevel
            
    def new_json(file_path : str, content : dict) -> None: # Запись в JSON файл без сохранение старого содержимого
        with open(file_path, 'w', encoding='utf-8') as file:
            json.dump(content, file, ensure_ascii=False, indent=3)
    
    def add_json(file_path : str, content : list) -> None: # Запись в JSON файл с сохранение старого содержимого
        try:
            with open(file_path, 'r', encoding='utf-8') as file:
                old_content = json.load(file)
        except Exception:
            print(f"File {file_path} don`t exist")
            old_content = []
        with open(file_path, 'w', encoding='utf-8') as file:
            json.dump(content + old_content , file, ensure_ascii=False, indent=3)

    def multiSplit(line : str, listSpliter : list) -> list:
        cache = []
        l_index = 0
        for c_index in range(len(line)):
            a = line[c_index]
            if line[c_index] in listSpliter:
                cache.append(line[l_index:c_index].lstrip())
                l_index = c_index+1
        return cache
    
    
    def getParts(text : str, subStr = "\n") -> list:
        def dropSpaces(line) -> str:
            for item in line:
                if item != ' ':
                    return line
            return " "
            
        subLength = len(subStr)
        parts = []
        partsIndexs = [_.start() for _ in re.finditer(subStr, text)]
        for index in range( len(partsIndexs)-1 ):
            test = text[partsIndexs[index]+subLength : partsIndexs[index+1]]
            # parts.append( dropSpaces( text[partsIndexs[index]+subLength : partsIndexs[index+1]] ) )
            parts.append( ( text[partsIndexs[index]+subLength : partsIndexs[index+1]] ) )
        return parts
    
    def getParagraphs(text : str) -> list:
        
        def refractParagraphs(parargraphs : list) -> list:
            cache = []
            for item in parargraphs:
                if item != '':
                    cache.append(item.lstrip().split(";"))
            return cache
        text = '\n' + text
        
        parts = OccurrencesCounter.getParts(text)
        parts.append(' ')
        parargraphs = []
        subParargraph = ''
        for index in range(len(parts)-1):
            if parts[index] == ' ':
                if parts[index+1] == ' ':
                    parargraphs.append(subParargraph)
                    subParargraph = ''
                else:
                    subParargraph += ' '
            else:
                if parts[index+1] == ' ':
                    subParargraph += parts[index]
                else:
                    if parts[index][-2:] == 'г.':
                        parargraphs.append(subParargraph)
                        subParargraph = ''
                    else:
                        subParargraph += parts[index]
            
        parargraphs = refractParagraphs(parargraphs)
        return parargraphs
                
    def setPDFTitle(self, pdfTitle : str) -> None:
        self.pdfTitle = pdfTitle.split()
    
    def occurrencePDF(self, pdfPath : str) -> None:
        with open(pdfPath, 'rb') as pdf_file:
            # print(pdf_file.name)
            pdf_reader = PyPDF2.PdfReader(pdf_file)     # Создаем объект PDF-документа
            num_pages = len(pdf_reader.pages)
            pages_output = []
            if num_pages >= 15:
                num_pages = 15                 # Получаем количество страниц в PDF-документе
            text = ""                                       # Создаем пустую строку для хранения текста
            for page in range(num_pages):                   # Проходим по всем страницам PDF-документа
                pdf_page = pdf_reader.pages[page]       # Получаем объект страницы
                page_text = pdf_page.extract_text()          # Извлекаем текст из страницы
                paragraphsText = OccurrencesCounter.getParagraphs(page_text)
                
                for element in paragraphsText:
                    for item in element:
                        for index in range( len(self.jsonData) - 1 ):
                        
                            fuzz_rate = fuzz.UWRatio(self.pdfTitle[index], item)
                            if fuzz_rate >= self.SIMILARITY_LEVEL :
                                self.jsonData['content'].append({'page_num' : page+1, 'rate' : fuzz_rate, 'description' : item})
                                if pdf_page not in pages_output:
                                    pages_output.append(pdf_page)
                                # print(item, page, fuzz_rate)

            pdf_writer = PyPDF2.PdfWriter()
            for page_num in pages_output:
                pdf_writer.add_page(page_num)
            
            just_name = str(pdf_file.name).split('\\')[-1]
            client_dir = 'front\pdf'
            parent = os.path.join(os.getcwd(), os.pardir) 
            client_path = parent[:-10] + client_dir
            client_path = os.path.join(client_path, just_name)
            # print(client_path)
                    
            client_path = client_path.replace('.pdf', 'output.pdf')
            with open(client_path, 'wb') as output_file:
                pdf_writer.write(output_file)
        

    def main(pdfTitle : str) -> list:
        localfilesNames = []
        for (dirpath, dirnames, filenames) in walk( OccurrencesCounter.ENTERY_POINT):
            localfilesNames.extend(filenames)
            break
        
        data = []
        
        for item in localfilesNames:
            data.append(OccurrencesCounter( OccurrencesCounter.ENTERY_POINT + "\\" + item, pdfTitle).getJsonData())
            
        return data
    
a = OccurrencesCounter.main("Cтроительство и обустройство скважин куста № 10 Гарюшкинского месторождения") # "occurrencesCounter\pdf_data\data.json"
print(a)
# "occurrencesCounter\pdf_data\Страницы из Капремонт.pdf"
# "Cтроительство и обустройство скважин куста № 10 Гарюшкинского месторождения"
# "Капитальный ремонт автомобильной дороги Р-215 Астрахань - Кочубей - Кизляр - Махачкала, подъезд к г. Грозный на участке км 70+127 – км 85+267, Чеченская Республика"
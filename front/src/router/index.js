import { createRouter, createWebHistory } from "vue-router";

import HomePage from '../Pages/HomePage'
import DownloadPage from '../Pages/DownloadPage'
import ResultPage from '../Pages/ResultPage'
import ClassificationPage from '../Pages/ClassificationPage'
import OneClassifications from '../Pages/OneClassifications'

const routes =  [ {path : '/',name:'start', component : HomePage },
                  {path : '/download',name:'download', component : DownloadPage},
                  {path:'/classification', name:'classification', component:ClassificationPage},
                  {path : '/result', name:'result', component : ResultPage},
                  {path:'/oneclassification', name:'oneclassification', component:OneClassifications},
                ]


const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router